from __future__ import division
import heapq

class Question2_Solver:
    def __init__(self):
        self.learn('train.data');
        return;

    # Add your code here.
    # Read training data and build your naive bayes classifier
    # Store the classifier in this class
    # This function runs only once when initializing
    # Please read and only read train_data: 'train.data'
    def learn(self, train_data):
        # print dataFormat
        with open(train_data) as f:
            content = f.readlines()

        (result, all_features) = ([i.split('\t', 1)[0] for i in content], [i.split('\t', 1)[1] for i in content])
        all_features = [i.split('\n', 1)[0] for i in all_features]

        classes = {}
        counts = {}

        self.prior = {}
        self.conditional = {}

        total = 0;
        for l in range(0, len(all_features)):
            fields = all_features[l].split(',');
            category = result[l];
            total += 1
            classes.setdefault(category, 0);
            counts.setdefault(category, {});
            classes[category] += 1
            col = 0
            for f in fields:
                col += 1
                counts[category].setdefault(col, {})
                counts[category][col].setdefault(f, 0);
                counts[category][col][f] += 1
        '''
        final_fields = []
        # set ? to majority in category. Look more closely into this
        for l in range(0, len(all_features)):
            fields_str = all_features[l]
            index = -1
            try:
                index = fields_str.index('?')
            except ValueError:
                final_fields.append(fields_str)
                continue

            fields = fields_str.split(',')

            for r in range(0, len(counts['democrat'])):
                dem_y = counts['democrat'][r+1]['y'] if 'y' in counts['democrat'][r+1] else 0
                dem_n = counts['democrat'][r+1]['n'] if 'n' in counts['democrat'][r+1] else 0
                rep_y = counts['republican'][r+1]['y'] if 'y' in counts['republican'][r+1] else 0
                rep_n = counts['republican'][r+1]['n'] if 'n' in counts['republican'][r+1] else 0
                if dem_y + rep_y + dem_n + rep_n == len(all_features):
                    continue;
                elif dem_y + rep_y >= dem_n + rep_n:
                    counts[result[l]][r+1]['y'] +=1
                else:
                    counts[result[l]][r+1]['n'] +=1 '''

        #print counts['republican'];
        #print counts['democrat'];
        #print classes
        #print total

        for (category, cnt) in classes.items():
            #print category, cnt, total
            self.prior[category] = (cnt) / (total );

        #print self.prior

        #laplase smoothing
        k = 4
        for (category, columns) in counts.items():
            self.conditional.setdefault(category, {})
            for (col, val) in columns.items():
                self.conditional[category].setdefault(col, {})
                for (attr, cnt) in val.items():
                    self.conditional[category][col][attr] = ((cnt + k) / (classes[category] + k*2))

        #print self.conditional

        return;

    # Add your code here.
    # Use the learned naive bayes classifier to predict
    # query example: 'n,y,n,y,y,y,n,n,n,y,?,y,y,y,n,y'
    # return 'republican' or 'republican'
    def solve(self, query):
        result = []

        for (category, prior) in self.prior.items():
            prob = prior
            col = 1
            for f in query.split(','):

                if not f in self.conditional[category][col]:
                    prob = 0;
                else:
                    #print prob
                    prob = prob*self.conditional[category][col][f]
                    col += 1
            result.append((prob, category))
        #print result
        #print ( max( result )[1] )
        #print [i[0] for i in result]
        if result[0][0] == result[1][0]:
            return "democrat"
        else:
            return ( max( result )[1] )


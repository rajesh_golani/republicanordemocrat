import math
class Question3_Solver:
    def __init__(self):
        return;

    # Add your code here.
    # Return the centroids of clusters.
    # You must use [(30, 30), (150, 30), (90, 130)] as initial centroids
    def solve(self, points):
        centroids = [(30, 30), (150, 30), (90, 130)];
        p1 = []
        p2 = []
        p3 = []
        centroids1 = list(centroids)
        change = 0;
        for i in range(0, 100):
            p1 = []
            p2 = []
            p3 = []
            for p in points:
                d1 = self.getDistance(centroids1[0], p)
                d2 = self.getDistance(centroids1[1], p)
                d3 = self.getDistance(centroids1[2], p)
                m = min(d1, d2, d3);
                #print d1, d2, d3
                '''if m == d1:
                    p1.append(p)
                elif m == d2:
                    p2.append(p)
                else:
                    p3.append(p)
                '''
                if d1 < d2:
                    if d3 < d1:
                        #print 2
                        p3.append(p)
                    else:
                        #print 0
                        p1.append(p)
                elif d3 < d2:
                    #print 2
                    p3.append(p)
                else:
                    #print 1
                    p2.append(p)

            del centroids1[:]
            sum_x1 = sum([j[0] for j in p1])
            sum_y1 = sum([j[1] for j in p1])
            #print (sum_x1/len(p1), sum_y1/len(p1))
            centroids1.append((sum_x1/len(p1), sum_y1/len(p1)))

            sum_x2 = sum([j[0] for j in p2])
            sum_y2 = sum([j[1] for j in p2])
            #print (sum_x2/len(p2), sum_y2/len(p2))
            centroids1.append((sum_x2/len(p2), sum_y2/len(p2)))

            sum_x3 = sum([j[0] for j in p3])
            sum_y3 = sum([j[1] for j in p3])
            #print (sum_x3/len(p3), sum_y3/len(p3))
            centroids1.append((sum_x3/len(p3), sum_y3/len(p3)))

            #print centroids1, centroids

            if centroids[0] == centroids1[0] and centroids[1] == centroids1[1] and centroids[2] == centroids1[2]:
                change += 1
            else:
                change = 0
            #print change
            if change == 3:
                break;

            #print "old", centroids
            del centroids[:]
            centroids = list(centroids1)
        return centroids1;

    def getDistance(self, p1, p2):
        d = math.pow( math.pow (( p2[0] - p1[0]), 2) + math.pow (( p2[1] - p1[1]), 2),  0.5)

        return d
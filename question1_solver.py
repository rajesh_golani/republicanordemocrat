from __future__ import division
import math

class Question1_Solver:
    def __init__(self):
        self.result = ['y', 'n', '?']
        self.learn('train.data')
        return

    def entropy(self, n, d):
        if n == 0 and d == 0:
            return 0

        pos_gain_prob = (n / (n + d))
        neg_gain_prob = (d / (n + d))
        if n == 0:
            temp = -1.0 * (d / (n + d)) * math.log(neg_gain_prob, 2)
            return temp
        elif d == 0:
            temp = -1.0 * (n / (n + d)) * math.log(pos_gain_prob, 2)
            return temp
        pos_gain = ((n / (n + d)) * math.log(pos_gain_prob, 2));
        neg_gain = ((d / (n + d)) * math.log(neg_gain_prob, 2));
        gain = -1.0 *(pos_gain + neg_gain);
        return gain

    def getDataInRequiredFormat(self, result, data, col):
        y = []
        n = []
        q = []
        val = []
        for ind, atl in enumerate(data):
            if atl[col] == 'y':
                y.append(atl)
            elif atl[col] == 'n':
                n.append(atl)
            elif atl[col] == '?':
                q.append(atl)

        val.append(y)
        val.append(n)
        val.append(q)

        y1 = []
        n1 = []
        q1 = []
        val1 = []
        for ind, atl in enumerate(data):
            if atl[col] == 'y':
                y1.append(result[ind])
            elif atl[col] == 'n':
                n1.append(result[ind])
            elif atl[col] == '?':
                q1.append(result[ind])

        val1.append(y1)
        val1.append(n1)
        val1.append(q1)
        return [val1, val]

    def learn(self, train_data):
        rows = []
        with open(train_data, "r") as f:
            data = f.readlines()
        (result, all_features) = ([i.split('\t', 1)[0] for i in data], [i.split('\t', 1)[1] for i in data])

        features = []
        for f in all_features:
            features.append(f.split('\n')[0].split(','))

        self.root = self.build_ID3_decision_tree(range(0, 16), result, features)
        self.root.preorder(self.root, 0, 0, -1)
        return

    # Add your code here.
    # Use the learned decision tree to predict
    # query example: 'n,y,n,y,y,y,n,n,n,y,?,y,y,y,n,y'
    # return 'republican' or 'republican'
    def solve(self, query):
        result = self.root.traversal(query.split(','))
        return result

    def build_ID3_decision_tree(self, features, majority, d):
        entropy = []
        indexes = []
        size = len(d)
        for f in features:
            sum = 0
            for r in self.result:
                dummy = []
                c = 0
                for index, yes_no in enumerate(d):
                    if yes_no[f] == r:
                        dummy.append(majority[index])
                        c += 1
                prob = c / size
                rep_count = dummy.count('republican')
                dem_count = dummy.count('democrat')
                entro = self.entropy(rep_count, dem_count)
                sum += prob * entro
            new_rep = majority.count('republican');
            dem_rep = majority.count('democrat')
            final_entro = self.entropy( new_rep, dem_rep) - sum;
            entropy.append(final_entro)
            indexes.append(f)
        maximum_gain_feature = indexes[entropy.index(max(entropy))]

        parsed_data = self.getDataInRequiredFormat(majority, d, maximum_gain_feature )

        node = Node(maximum_gain_feature)
        features.remove(maximum_gain_feature)

        for i, r in enumerate(self.result):
            demo_count = parsed_data[0][i].count('democrat');
            rep_count = parsed_data[0][i].count('republican');

            if demo_count == 0 and rep_count == 0:
                if majority.count('republican') >= majority.count('democrat'):
                    republican = Node('republican')
                    node.add_child(republican)
                else:
                    democrat = Node('democrat')
                    node.add_child(democrat)
            elif demo_count == 0:
                republican = Node('republican')
                node.add_child(republican)
            elif rep_count == 0:
                democrat = Node('democrat')
                node.add_child(democrat)
            else:
                new_node = self.build_ID3_decision_tree(features, parsed_data[0][i], parsed_data[1][i])
                node.add_child(new_node)
        return node

class Node(object):
    def __init__(self, identifier):
        self.identifier = identifier
        self.childs = []

    def add_child(self, obj):
        self.childs.append(obj)

    def traversal(self, query):
        node = self
        identifier = node.identifier
        while (1):
            decision = query[identifier]
            if decision == '?':
                node = node.childs[2]
            elif decision == 'y':
                node = node.childs[0]
            elif decision == 'n':
                node= node.childs[1]
            identifier = node.identifier
            if identifier == 'democrat' or identifier == 'republican':
                break
        return node.identifier

    def preorder(self, node, level, child, parent):
        if node is None:
            return
        #print '->',node.identifier, level, child, parent
        for i in range(0, 3):
            n = None
            try:
                n = node.childs[i]
            except IndexError:
                n = None
            self.preorder(n, level+1, i, node.identifier)